#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:17.10 alfresco
sleep 3

./init-container.sh alfresco
ansible-playbook -vvvv playbooks/deploy-alfresco.yml -i hosts -l alfresco

./init-nginx.sh alfresco
sudo service nginx restart

lxc restart alfresco

popd