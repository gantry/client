
NAME=$1

pushd ../ansible

echo "Creating $NAME"
lxc launch ubuntu:16.04 $NAME

echo "Mounting local /opt to container /mnt"
lxc config device add $NAME sharedtmp disk path=/mnt source=$(pwd)

echo "Spawning Shell, type exit to break out"
lxc exec $NAME -- bash

echo "Exiting Shell"
echo "To respawn, use : "
echo "lxc exec $NAME -- bash"

popd





