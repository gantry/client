#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:16.04 gitlab
sleep 3
./init-container.sh gitlab
ansible-playbook -vvvv playbooks/deploy-gitlab.yml -i hosts -l gitlab

./init-nginx.sh gitlab
sudo service nginx restart
lxc restart gitlab

popd