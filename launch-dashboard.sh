#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:17.10 dashboard
sleep 3
./init-container.sh dashboard
ansible-playbook -vvvv playbooks/deploy-dashboard.yml -i hosts -l dashboard

./init-nginx.sh dashboard

sudo service nginx restart
lxc restart dashboard

popd