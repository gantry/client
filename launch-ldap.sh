#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:16.04 ldap
sleep 3
./init-container.sh ldap
ansible-playbook -vvvv playbooks/deploy-ldap.yml -i hosts -l ldap

./init-nginx.sh ldap
sudo service nginx restart
lxc restart ldap

popd