#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:18.04 wordpress
sleep 3
./init-container.sh wordpress
ansible-playbook -vvvv playbooks/deploy-wordpress.yml -i hosts -l wordpress

popd
