#!/usr/bin/env bash

pushd ../ansible

lxc launch ubuntu:16.04 jira
sleep 3
./init-container.sh jira
ansible-playbook -vvvv playbooks/deploy-jira.yml -i hosts -l jira

./init-nginx.sh jira
sudo service nginx restart
lxc restart jira

popd